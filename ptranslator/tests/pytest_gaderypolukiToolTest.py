import pytest

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator


# iteracja i kolejne wywołania testów
@pytest.mark.parametrize("test_input, expected", [
    ("Ala", "Gug"),
    ("la", "ug"),
    ("kot", "ipt"),
    ("wonsz", "wpnsz"),
    ("KwieCIEN", "IwkdCKDN")
])
def test_should_translate(translator, test_input, expected):
    assert translator.translate(test_input) == expected

def test_should_check_translatable(translator):
    # given
    c = "P"

    # when
    result = translator.is_translatable(c)

    # then
    assert result