import unittest

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


class GaDeRyPoLuKiToolTest(unittest.TestCase):
    g = None

    def setUp(self):
        self.g = GaDeRyPoLuKi()
        pass

    def tearDown(self):
        self.g = None
        pass

    # definicja tablicy krotek, które stanowią wejście do jednego testu
    # każda krotka zawiera wiadomość do przetłumaczenia i oczekiwaną wartość
    test_cases = [
        ("Ala", "Gug"),
        ("la", "ug"),
        ("kot", "ipt"),
        ("wonsz", "wpnsz"),
        ("KwieCIEN", "IwkdCKDN")
    ]

    # iteracja i kolejne wywołania testów
    def test_should_translate(self):
        for msg, expected in self.test_cases:
            with self.subTest(name=str(msg)):
                result = self.g.translate(msg)
                self.assertEqual(result, expected)

    test_cases_translatable_letters = ("G", "U", "a")

    def test_should_check_translatable(self):
        for msg in self.test_cases_translatable_letters:
            with self.subTest(name=str(msg)):
                result = self.g.is_translatable(msg)
                self.assertTrue(result)

