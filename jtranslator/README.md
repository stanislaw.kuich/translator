# Jtranslator
Jtranslator jest implementacją translatora gaderypoluki w Javie, Springu i Apache CXF. Zawiera logikę biznesową w formie serwisu oraz kontrolery, za pomocą których można wywołać ten serwis. 
## Technologie
* Java8 lub nowsza
* JUnit 5
* Junit 4
* Spring/SpringBoot
* Apache CXF
* REST, SOAP
## Moduły
 * translator - logika biznesowa
 * rest - moduł obsługi żądań REST
 * ws - moduł obsługi żądań SOAP/WebService
## Uwagi techniczne
### Preferowane IDE
IntelliJ
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE.
### Uruchamianie aplikacji
Z wykorzystaniem IDE, osobno moduł `rest` oraz `ws` lub też z wykorzystaniem dockera.
#### Docker
Aplikacja jest zdockeryzowana w postaci dwóch obrazów i dwóch kontenerów, osobno `rest` i `ws`. Normalnie aplikacje SpringBoot są uruchamiane na porcie 8080, dlatego przy uruchomieniu obu aplikacji jednocześnie (`rest` i `ws`) usługi są serwowane na dwóch portach, odpowiednio 8082 i 8081.

Komendy:

```
docker-compose build
docker-compose up
```

### Zapytania REST
`curl http://localhost:8080/gaderypoluki/napisTestowy` lub dla dockera:

`curl http://localhost:8082/gaderypoluki/napisTestowy`

### Zapytania WS
`curl --header "content-type: text/xml" -d @request.xml http://localhost:8080/services/gaderypoluki` lub dla dockera:

`curl --header "content-type: text/xml" -d @request.xml http://localhost:8081/services/gaderypoluki`

Można także wykorzystać osobny program **SoapUI**, który zasilamy generowanym plikiem wsdl. Plik można wygenerować żądaniem:
`http://localhost:8080/services/gaderypoluki?wsdl`