package pl.poznan.put.spio.web;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.response.MockMvcResponse;
import io.restassured.path.json.JsonPath;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.poznan.put.spio.controller.GaDeRyPoLuKiController;
import pl.poznan.put.spio.controller.ErrorHandler;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GaDeRyPoLukiWebTest {

    @Autowired
    private GaDeRyPoLuKiController controller;

    @BeforeEach
    public void setUp() {
        // configure mockMvc and instrument RestAssured
        final MockMvc mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
                .setControllerAdvice(new ErrorHandler())
                .build();
        RestAssuredMockMvc.mockMvc(mockMvc);
    }


    /**
     * This translation will fail, because translation key has 13 characters.
     */
    @Test
    public void shouldGetBadRequestOnInternalException() {
        // given

        // when
        given()
                // some place for additional config
                .when()
                // the request
                .get("/gaderypoluki/{msg}/with/{key}", "kot", "akaceminutowy")
                // then expects
                .then()
                // let's assertions begin
                .assertThat()
                // check the code
                .statusCode(HttpStatus.BAD_REQUEST.value());

        // then
    }

    @Test
    public void shouldTranslate() {
        // given

        // when
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}", "kot", "kaceminutowy")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                // extract the response
                .extract()
                .response();

        // then
        // check JSON response
        // mixed with AssertJ
        Assertions.assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("ato");
    }

    @Test
    public void shouldTranslate2() {
        // given

        // when
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}", "kot", "politykaremu")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                // extract the response
                .extract()
                .response();

        // then
        // check JSON response
        // mixed with AssertJ
        Assertions.assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("apy");
    }

    @Test
    public void shouldTranslate3() {
        // given

        // when
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}", "kot")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                // extract the response
                .extract()
                .response();

        // then
        // check JSON response
        // mixed with AssertJ
        Assertions.assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("ipt");
    }

    @Test
    public void shouldTranslate_ignorecase() {
        // given

        // when
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/ignorecase", "KwieCIEN")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                // extract the response
                .extract()
                .response();

        // then
        // check JSON response
        // mixed with AssertJ
        Assertions.assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("IwkdCKDN");
    }

    @Test
    public void shouldTranslate_ignorecase_with_key() {
        // given

        // when
        final MockMvcResponse response = given()
                .when()
                .get("/gaderypoluki/{msg}/with/{key}/ignorecase", "KOt", "politykaremu")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                // extract the response
                .extract()
                .response();

        // then
        // check JSON response
        // mixed with AssertJ
        Assertions.assertThat(JsonPath.from(response.asString()).get("translated").toString()).isEqualTo("APy");
    }

}
