package pl.poznan.put.spio.ws;

import lombok.Data;

@Data
public class Request {
    private String message;
}
