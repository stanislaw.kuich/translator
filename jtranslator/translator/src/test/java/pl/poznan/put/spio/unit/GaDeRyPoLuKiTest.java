package pl.poznan.put.spio.unit;


import org.junit.jupiter.api.*;
import pl.poznan.put.spio.translator.GaDeRyPoLuKi;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
public class GaDeRyPoLuKiTest {

    private GaDeRyPoLuKi g;

    /**
     * Uporządkowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
     * dla całej klasy testującej, po zakończeniu wszystkich testów @Test.
     */
    @AfterAll
    static void cleanUpClass() {
        // not this time
    }

    /**
     * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana tylko raz,
     * dla całej klasy testującej, przed rozpoczęciem jakiegolwiek testu @Test.
     */
    @BeforeAll
    static void initClass() {
        // not this time
    }

    /**
     * Uporządkowanie środowska testowego JUnit. Metoda uruchamiana po każdym
     * przypadku testowym @Test.
     */
    @AfterEach
    void cleanUp() {
        g = null;// odrobinę nadmiarowe zwolnienie zasobów
    }

    /**
     * Przygotowanie środowiska testowego JUnit. Metoda uruchamiana przed każdym
     * przypadkiem testowym @Test.
     */
    @BeforeEach
    void init() {
        g = new GaDeRyPoLuKi();// utworzenie obiektu translatora
    }

    /**
     * Test translacji, gdy litery nie są przewidziane w mapie translacji.
     */
    @Test
    void shouldStayNotTranslated() {
        // given
        final String msg = "LOK";

        // when
        final String result = g.translate(msg);// translacja, wynik zapisany w
        // 'result'

        // then
        fail("TODO");
    }

    /**
     * Podany w teście argument o wartości null spowoduje rzucenie wyjątku
     * {@link NullPointerException}, który w tym miejscu jest oczekiwany.
     */
    @Test
    public void shouldThrowExceptionWhenTranslateNull() {
        // when
        assertThrows(NullPointerException.class, () -> {
            g.translate(null);// tylko translacja, powinna rzucić wyjątek
        });
    }

    /**
     * Przykładowy test sprawdzania translacji napisu.
     */
    @Test
    void shouldTranslate() {
        // given
        final String msg = "lok";

        // when
        final String result = g.translate(msg);// translacja, wynik zapisany w
        // 'result'

        // then
        assertEquals("upi", result);// metoda sprawdzająca
    }

    /**
     * Test translacji, ignorowana wielkość liter.
     */
    @Test
    void shouldTranslateIgnoreCase() {
        // given
        final String msg = "KOT";

        // when
        final String result = g.translateIgnoreCase(msg);

        // then
        fail("TODO");
    }

    /**
     * Sprawdzenie długości kodu do translacji.
     */
    @Test
    void shouldCheckCodeLength() {
        // given

        // when
        int size = g.getCodeLength();

        // then
        fail("TODO");
    }

    /**
     * Sprawdzenie czy dany znak będzie podlegał translacji.
     */
    @Test
    void shouldCheckIsTranslatable1() {
        // given
        final String c = "Z";

        // when
        final boolean result = g.isTranslatable(c);

        // then
        assertFalse(result);
    }

    /**
     * Sprawdzenie czy dany znak będzie podlegał translacji.
     */
    @Test
    void shouldCheckIsTranslatable2() {
        // given
        final String c = "g";

        // when
        final boolean result = g.isTranslatable(c);

        // then
        fail("TODO");
    }

    /**
     * Test translacji, kolejne litery do sprawdzenia.
     */
    @Test
    void testTranslate3() {
        // given

        // when
        final String result = g.translate("chomik");// translacja, wynik zapisany
        // w 'result'

        // then
        fail("TODO");
    }
}
